var birthcounty = {};
birthcounty['Tehran'] = ['Malard', 'Ghods', 'Eslamshahr'];
birthcounty['Eastazerbaijan'] = ['Tabriz', 'Maraghe', 'Marand'];
birthcounty['Mazandaran'] = ['Amol', 'Babol', 'Ramsar'];
birthcounty['Yazd'] = ['Ardalan', 'Meibod', 'Anar'];
birthcounty['Fars'] = ['Fasa', 'Marvdasht', 'Jahrom'];

function Changestatelist() {

    var countylist = document.getElementById("inputcounty");

    var countyselect = $("#inputstate").val();

    while (countylist.options.length) {
        countylist.remove(0);
    }

    var birthplace = birthcounty[countyselect];

    if (birthplace) {
        var i;

        for (i = 0; i < birthplace.length; i++) {

            var x = new Option(birthplace[i]);

            countylist.options.add(x);
        }
    }
}