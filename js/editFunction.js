var selectedclass = "";
var y = "";
//وقتی روی ادیت کلیک کردی 
$(document).on('click', 'button.edit', function editbtn() {
    //هر سلول از جدول را میگیرد و متنش را داخل ورودی میریزد
    //$(getname) = $(this).parents("tr").find("td:eq(0)").text();
    //اتریبیوت data-... را بگیر 

    y = $(this).closest('tr').data("number");
    var getname = $(this).parents("tr").data("name");
    $("#inputname").val(getname);
    var getfamily = $(this).parents("tr").data("family");
    $("#inputfamily").val(getfamily);
    var getdate = $(this).parents("tr").data("age");
    $("#inputage").val(getdate);
    var getsport = $(this).parents("tr").data("sport");
    $("#inputsport").val(getsport);
    var getnumber = $(this).parents("tr").data("number");
    $("#inputnumber").val(getnumber);
    var getbirthsatate = $(this).parents("tr").data("birthsatate");
    $("#inputstate").val(getbirthsatate);
    var getbirthcounty = $(this).parents("tr").data("birthcounty");
    $("#inputcounty").val(getbirthcounty);
    var getdes = $(this).parents("tr").data("description");
    $("#inputdescription").val(getdes);
    var getvaluegender = $(this).parents("tr").data("gender");
    $("input[name=genderRadios][value=" + getvaluegender + "]").prop('checked', true);

    //از var استفاده نمیکنیم چون یکبار در بالامعرفی کردیم که dynamic یک متغیر است
    //selectedclass = $(this).parents("tr");
    selectedclass = $(this).parents("tr");
    //کلاس selected را به سطر میدهد
    //داخل دلار ساین قرار میدهیم چون یک variable است
    $(selectedclass).addClass("selected");
    // $(selectedclass).attr("data-number",getnumber);

    $("#myModalHorizontal").modal('show');

    //دو تا دکمه save و cancel اضافه شود.
    $("#submit").parent("div").append(`<button type="submit" class="btn btn-primary save" style="margin-right: 15px; display:inline-block">save</button><button type="submit" class="btn btn-default cancel" style="display:inline-block">cancel</button>`)
        //تغییر دکمه ی submit به save
        //و تغییر آیدی دکمه به save
        //$("#submit").text('Save');
        // $('#submit').attr('id', 'save');

    //دکمه submit مخفی شود.
    $("#close").hide();
    $(".edit").hide();
    $("#submit").hide();
    $(".delete").hide();

});