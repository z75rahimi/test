function createTable(x) {
    //به ازای هر سری محتوای جدول را خالی کن تا هر سری اطلاعات جدید که در ورودی اضافه میکنیم با اطلاعات قبلی نمایش داده نشود پس به خاطر حلقه for این خط رو اضافه کردیم
    $("#table tbody").html("");

    //وقتی از localstorage استفاده نمیکردیم اطلاعات را در userarrey که به صورت آرایه بود سیو میکردیم -بهجای ایکس-و
    //اگر یکبار فرم را پر کنیم و سابمیت رو بزنیم اطلاعات داخل آبجکت میریزن که همونuser نام داره و در نهایت همه این آبجکت ها داخل یک آرایه به نامuserarray ریخته میشوند
    //به خاطر همین اگه بگیمuserarray[0] یعنی کل فرمت user برای اولین فرم داخل آرایه ی صفرم از userarray میریزن
    //چون مقدار localStorage.getItem('users') یک استرینگ است به جیسان تبدیل میکنیم تا بتونیم از متغیرها که همان کی هستند و مقدارشان که ولیو هستند استفاده کنیم یعنی ما اطلاعات را از localstorage میخوانیم
    //var x = JSON.parse(localStorage.getItem('users'));

    //برای هر ایندکسی که توی آبجکت x است یعنی هرچند بارکه فرم رو پر کردیم
    for (var i in x) {
        //برای ایکس صفرم،یکم، دوم و تا هرجایی که فرم رو سابمیت کردی تابع createRow رو فراخوانی کن.
        createRow(x[i]);
    }

    //عدد برایشان تنظیم کن
}

//تابعی که اطلاعات رو از ورودی میگیره و داخل جدول میریزه و دو تا دکمه ادیت و حذف رو اضافه میکنه
function createRow(data) {
    $("#table tbody").append(`<tr><td></td><td>${data.nameuser}</td>
<td>${data.familyuser}</td>
<td>${getAge(data.ageuser)}</td>
<td>${data.genderuser}</td>
<td>${data.sportuser}</td>
<td>${data.numberuser}</td>
<td>${data.birthstateuser}</td>
<td>${data.birthcountyuser}</td>
<td>${data.descriptionuser}</td> 
<td><button class="btn btn-danger delete">
<i class="glyphicon glyphicon-trash"></i></button>
<button class="btn btn-success edit">
<i class="glyphicon glyphicon-pencil"></i></button></td></tr>`);

    //attribute به آخرین tr اضافه کن مقادیر attribute ها همان اطلاعاتی است که کاربر وارد کرده است
    //$("tr").last().data("count", count);
    $("tr").last().data("name", data.nameuser);
    $("tr").last().data("family", data.familyuser);
    $("tr").last().data("age", data.ageuser);
    $("tr").last().data("gender", data.genderuser);
    $("tr").last().data("sport", data.sportuser);
    $("tr").last().data("number", data.numberuser);
    $("tr").last().data("birthsatate", data.birthstateuser);
    $("tr").last().data("birthcounty", data.birthcountyuser);
    $("tr").last().data("description", data.descriptionuser);
}