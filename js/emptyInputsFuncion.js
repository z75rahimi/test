function emptyinputs() {

    $("#inputname").val('');
    $("#inputfamily").val('');
    $("#inputage").val('');
    $("#inputsport").val('');
    $("#inputdescription").val('');

    $("#inputnumber").val('');
    $("#inputstate").val('');
    $("#inputcounty").val('');
    $("input[name=genderRadios]").prop("checked", false);
}