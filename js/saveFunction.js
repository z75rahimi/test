$(document).on("click", ".save", function() {
    //مقادیر ورودی رو بگیر 
    var returnname = $("#inputname").val();
    var returnfamilyname = $("#inputfamily").val();
    var returnage = $("#inputage").val();
    var returnsport = $("#inputsport").val();
    var returnbirthsatate = $("#inputstate").val();
    var returnbirthcounty = $("#inputcounty").val();
    var returnnumber = $("#inputnumber").val();
    var returndes = $("#inputdescription").val();

    //object جدید تعریف میکنیم ولی متغیرهای داخل را تغییر نمیدهیم و مثل قبل تعریف میکنیم
    var userinfo = {
        "nameuser": returnname,
        "familyuser": returnfamilyname,
        "ageuser": returnage,
        "genderuser": $('input[name=genderRadios]:checked').val(),
        "sportuser": returnsport,
        "birthstateuser": returnbirthsatate,
        "birthcountyuser": returnbirthcounty,
        "numberuser": returnnumber,
        "descriptionuser": returndes,
    };
    //validage(userinfo);
    // validnumber();
    //save را به submit تغییر میدهیم
    //$(this).text("submit");
    //$(this).attr('id', 'submit');
    //validation را برای آبجکت جدید تعریف میکنیم

    //اگر تابع کلی ترو بود
    if (totalvalidation1(userinfo)) {
        //اگر محتوا داشت فانکشن را اجرا کن
        //userarray[i] = userinfo;

        //userarray.push(userinfo);

        //localStorage.setItem('users', JSON.stringify(userarray));

        // اطلاعات اپدیت شده را در ایکس بریز
        var x = JSON.parse(localStorage.getItem('users'));

        //var x = JSON.parse(localStorage.getItem('users'));


        //برای تعداد ایندکس هایی که در ابجکت ایکس است
        for (var i = 0; i < x.length; i++) {
            //اگر مقدار عدد ابجکت برابر y بود
            if (x[i].numberuser == y) {
                //ابجکت قبلی را با ابجکت جدید جابجا کن
                x[i] = userinfo;
            }
        }
        //و یکبار دیگر لوکال را اپدیت کن
        localStorage.setItem('users', JSON.stringify(x));
        //و به جیسان تبدیل کن
        x = JSON.parse(localStorage.getItem('users'));
        //برای هر جیسان ایکس جدول بساز

        addpagebutton1(x);
        addpagebutton(x);
        //createTable(x);
        $("#myModalHorizontal").modal('hide');
        $("#submit").show();
        $(".close").show();
        $(".save").remove();
        $(".cancel").remove();
        $(".edit").show();
        $(".delete").show();


        //مقدار ورودی ها را خالی میکنیم
        emptyinputs();
        //کلاس selected را برمیداریم
        $(selectedclass).removeClass();
        // $(selectedclass).removeAttribute("data-number");
        //اگر محتوا نداشت
    }

});