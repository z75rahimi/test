$("#submit").on("click", function() {


    //تابع را فراخوانی کن که اگر لوکال خالی بود یک ارایه خالی را قرار بده
    // emptylocalstorage();
    //اطلاعات را از inputها میگیرد 
    var name = $("#inputname").val();
    var familyname = $("#inputfamily").val();
    var age = $("#inputage").val();
    var sport = $("#inputsport").val();
    var number = $("#inputnumber").val();
    var birthsatate = $("#inputstate").val();
    var birthcounty = $("#inputcounty").val();
    var des = $("#inputdescription").val();
    //object جدید تعریف کرد و ورودی ها رو تو اون ریخت 
    var user = {

        "nameuser": name,
        "familyuser": familyname,
        "ageuser": age,
        "genderuser": $('input[name=genderRadios]:checked').val(),
        "sportuser": sport,
        "numberuser": number,
        "birthstateuser": birthsatate,
        "birthcountyuser": birthcounty,
        "descriptionuser": des,
    };

    //شرط تعیین میکنه که اگه محتوا خالی بود و پر بود چی کار کن
    if (totalvalidation(user)) {
        //پیام بده که سابمیت شد


        Command: toastr["success"]("submit successfully")

            toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        //سطر جدید ایجاد کن
        //createRow(user);
        //هر وقت که محتوا پر بود هر user را که به صورت آبجکت یا (json) است را به آرایه تبدیل میکند
        //اطلاعات قبلی که وجود داشت را به جیسان تبدیل کن و در usrearray بریز
            var userarray = JSON.parse(localStorage.getItem('users'));
        //user که وارد کردی را به userarray اضافه کن
        userarray.push(user);

        //*************************localstorage : با آپدیت کردن صفحه اطلاعات در آن ذخیره میشود**************************
        //userarray که به صورت json یا آبجکت است را به string  تبدیل میکند        
        //یک متغیر دلخواه به نام users تعیین میکنیم و بهش مقدار استرینگ آبجکتمان را میدهیم
        //یکبار دیگر userarray را در لوکال ست کن
        localStorage.setItem('users', JSON.stringify(userarray));
        //حالا اطلاعات خط قبل که در لوکال سیو کردی را به جیسان تبدیل کن و در x بریز
        var x = JSON.parse(localStorage.getItem('users'));

        // $("#myModalHorizontal").modal("hide");

        addpagebutton1(x);
        addpagebutton(x);

        //حالا برای ایکس هایی که وجود داشت را جدول بساز
        //createTable(x);
        //بعد محتوا رو خالی کن
        emptyinputs();

        $("#submit").attr("data-dismiss", "modal");
    }
    else {
        $("#submit").attr("data-dismiss", "");
    }
});



function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);

    var age = today.getFullYear() - birthDate.getFullYear();

    var m = today.getMonth() - birthDate.getMonth();

    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}