// تابعی برای اینکه اگر سن کمتر بود ارور بدهد
function limitage(data) {
    //متغیری تعریف میکنیم که پیش فرضش ترو است
    //علت اینکه از متغیر استفاده میکنیم و از return استفاده نمیکنیم اینست که وقتی میخواهیم از ایف در جای دیگر هم استفاده کنیم نمیتوان از returnاستفاده کرد چون بقیه  شرطها اجرا نمیشود 
    var validage = true;
    //اگر کمتر از ۱۸ بود
    if (getAge(data.ageuser) < 18) {
        //متغیر را فالس کن
        validage = false;

        Command: toastr["error"]("age is low")

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

    }
    //مقدار تابع را برگردان(یعنی ترو یا فالس)
    return validage;
}

//تابعی برای اینکه اگر دو عددشبیه به هم بود
function equalnumber() {
    //از لوکال ولیو users که همان استرینگ است را بگیر و به جیسان تبدیل کن
    var x = JSON.parse(localStorage.getItem('users'));
    //متعبر با مقدار پیش فرض ترو در نظر میگیریم
    var validnumber = true;
    //برای i هایی که به تعداد ایندکس هایی که در ایکس است
    for (var i = 0; i < x.length; i++) {
        // مفدار input عدد را بگیر
        //اگر برابر اعدادموجود در ارایه ها بود
        if ($("#inputnumber").val() == x[i].numberuser) {
            //    متغیر را فالس کن
            validnumber = false;
            //پیام بده
            Command: toastr["error"]("exist two equal number")

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        }
    }

    //متغیر را برگردان
    return validnumber;
}


//تابعی تعریف میکنه که اگر محتوا خالی بود چیکار کنه
function validation(data) {
    //متغیر تعریف میکند که مقدار اولیه اش ترو است
    var validempty = true;
    // اگر خالی بود
    if (data.nameuser == "" || data.familyuser == "" || data.ageuser == "" || data.genderuser == undefined || data.sportuser == null || data.numberuser == "" || isNaN(data.numberuser) || data.birthstateuser == null || data.birthcountyuser == null) {

        //متغیر را فالس کن
        validempty = false;
        //پیام بده
        Command: toastr["error"]("there is issue on fields")

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }



    }
    //مقدار متغیر را برگردان
    return validempty;
}

//تابعی تعریف کن که همه شرایط را بررسی کن
function totalvalidation(data) {
    //متغیری تعریف میکنیم که مقدار تابع محدودیت سنی را بگیرد و برابر متغیر جدیدقرار بدهد
    var resultage = limitage(data);
    //متغیری تعریف میکنیم که مقدار تابع عدم برابری دو عدد را بگیرد و برابر متغیر جدیدقرار بدهد
    var resultnumber = equalnumber();
    //متغیری تعریف میکنیم که مقدار تابع خالی نبودن محتوا را بگیرد و برابر متغیر جدیدقرار بدهد
    var resultvalidation = validation(data);
    //اگر هر کدام از متغیرهای جدید فالس بود
    if (resultage == false || resultnumber == false || resultvalidation == false) {
        //فالس را برگردان
        return false;
    } {
        //در غیر اینضورت ترو کن
        return true;
    }

}


function totalvalidation1(data) {
    var resultage = limitage(data);
    //برای عدد تابع جدید تعریف میکنی که اگر اگر عدد برابر خود ابجکت بود ایراد نگیرد و مقدار تابع را برابر این متغیر قرار بده
    var resultnumber1 = equalnumber1();
    var resultvalidation = validation(data);

    //اگر یمی از شروط فالس بود فالس را برگردان و اجازه نده بقیه اجرا شود
    if (resultage == false || resultnumber1 == false || resultvalidation == false) {

        return false;

    } {
        return true;
    }
}
//برای برابری عدد تابع جدید تعریف میکنی که اگر اگر عدد برابر خود ابجکت بود ایراد نگیرد
function equalnumber1() {
    //یکبار دیگر اطلاعات اپدیت شده را در ایکس بریز
    var x = JSON.parse(localStorage.getItem('users'));
    //مقدار پیش فرض متغیر را برابر ترو کن
    var validnumber = true;
    //برای تعداد ایندکس هایی که در ارایه ایندکس هست
    for (var i = 0; i < x.length; i++) {
        //اگر برابر y یعنی مقدار data-number سطری مه ادیت میکنیم بود
        if (x[i].numberuser == y) {
            //ادامه بده و ان را بررسی نکن و ایراد نگیر
            continue;
        }
        // و اگر مقدار ورودی برابر اعداد هریک از ابجکت ها بود
        if ($("#inputnumber").val() == x[i].numberuser) {

            //متغیر را فالس کن
            validnumber = false;
            //پیام بده
            Command: toastr["error"]("exist two equal number")

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        }
    }
    //در تابع مقدار متغیر را برگردان
    return validnumber;
}